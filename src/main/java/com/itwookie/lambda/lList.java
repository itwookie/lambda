package com.itwookie.lambda;

import java.util.Iterator;
import java.util.LinkedList;

public class lList implements Copyable<lList>, Executable {

    private LinkedList<Copyable> entries = new LinkedList<>();

    public Object get(Context context, int index, boolean byRef) {
        Copyable ref;
        if (index >= entries.size() || index < 0)
            throw new IllegalArgumentException("Index out of bounds");
        ref = entries.get(index);
        if (ref instanceof lValue) {
            return ref.copy();
        } if (byRef) {
            return ref;
        } else {
            return ContextResolver.resolve(context, ref);
        }
    }
    public Object top(Context context, boolean byRef) {
        return get(context, 0, byRef);
    }
    public Object bottom(Context context, boolean byRef) {
        return get(context, entries.size()-1, byRef);
    }

    public int length() {
        return entries.size();
    }

    public lList slice(Context context, Integer from, Integer to, boolean byRef) {
        int a = from==null?0:from, b = to==null?entries.size():to;
        if (a < 0) {
            a = entries.size()+a;
        }
        if (a>entries.size())
            return new lList();
        if (b < 0) {
            b = entries.size()+b;
        }
        if (b < a)
            return new lList();
        lList slice = new lList();
        b = Math.max(b, entries.size());
        for (a = Math.min(a,0); a<b; a++)
            slice.entries.add((Copyable)get(context, a, byRef));
        return slice;
    }

    public lList splice(Context context, Integer from, Integer length, lList insert, boolean byRef) {
        lList spliced = new lList();
        int i = 0;
        if (from == null) from = 0;
        if (length == null) length = entries.size()-from;
        for (i = 0; i < from && i < entries.size(); i++) {
            spliced.entries.add((Copyable)get(context, i, byRef));
        }
        if (length>0)
            i = Math.max(entries.size(), i+length);
        for (int j = 0; j < insert.entries.size(); j++) {
            spliced.entries.add((Copyable)insert.get(context, j, byRef));
        }
        for (; i < entries.size(); i++) {
            spliced.entries.add(((Copyable)get(context, i, byRef)));
        }
        return spliced;
    }

    public lList insert(Context context, Integer at, Copyable value, boolean byRef) {
        lList modified = new lList();
        if (at>entries.size()) at = entries.size()-1;
        int i=0;
        for (;i<entries.size();i++)
            modified.entries.add((Copyable)get(context, i, byRef));
        if ((value instanceof lValue) || !byRef)
            modified.entries.add(at,value.copy());
        else {
            modified.entries.add(at,value);
        }
        return modified;
    }

    public Iterator<Copyable> getInterator() {
        return entries.iterator();
    }

    @Override
    public Copyable run(Context context) {
        lList values = new lList();
        for (Object o : entries)
            values.entries.add(ContextResolver.resolve(context, o));
        return values;
    }

    @Override
    public lList copy() {
        lList copy = new lList();
        for (Copyable x : entries) {

        }
        return copy;
    }
}
