package com.itwookie.lambda;

import com.itwookie.lambda.parser.Parser;

import java.io.*;

public class Runtime {

    public static void main(String[] args) throws IOException {
        File f = new File("test.lam");
        Runtime runtime = new Parser().parse(new FileInputStream(f));
    }

}
