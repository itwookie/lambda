package com.itwookie.lambda;

public class lValue implements Comparable<lValue>, Copyable<lValue> {

    private Object value;
    private lValue(String token) {
        value = token;
    }
    private lValue(Double number) {
        value = number;
    }
    private lValue(Integer number) {
        value = number;
    }

    public static lValue fromString(String string) {
        try {
            return new lValue(Double.parseDouble(string));
        } catch (NumberFormatException e) {
            /**/;
        }
        try {
            return new lValue(Integer.parseInt(string));
        } catch (NumberFormatException e) {
            /**/;
        }
        return new lValue(string);
    }

    public Number asNumber() throws NumberFormatException {
        if (value instanceof String) {
            try {
                Double.parseDouble((String)value);
            } catch (NumberFormatException e) {
                /**/;
            }
            try {
                Integer.parseInt((String)value);
            } catch (NumberFormatException e) {
                /**/;
            }
            throw new NumberFormatException();
        }
        else return (Number)value;
    }
    public double asDouble() throws NumberFormatException {
        return asNumber().doubleValue();
    }
    public int asInteger() throws NumberFormatException {
        return asNumber().intValue();
    }
    public byte asByte() throws NumberFormatException {
        return asNumber().byteValue();
    }
    public String asString() {
        return String.valueOf(value);
    }

    public boolean isNumeric() {
        try {
            asNumber();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static lValue ofString(String string) {
        return string == null ? VOID : new lValue(string);
    }
    public static lValue ofInteger(int value) {
        return new lValue(value);
    }
    public static lValue ofDouble(double value) {
        return new lValue(value);
    }
    public static lValue ofByte(byte value) {
        return new lValue(((int) value) & 0x0ff);
    }
    public static lValue of(Object value) {
        if (value == null)
            return VOID;
        else if (value instanceof String)
            return lValue.ofString((String) value);
        else if ((value instanceof Double) || (value instanceof Float))
            return lValue.ofDouble(((Number)value).doubleValue());
        else if (value instanceof Number) {
            return lValue.ofInteger(((Number)value).intValue());
        } else {
            throw new IllegalArgumentException("Type "+value.getClass().getSimpleName()+" not supported");
        }
    }

    /** debug output */
    @Override
    public String toString() {
        return value instanceof String ? '"'+((String)value)+'"' : asString();
    }

    public static lValue VOID = null;

    @Override
    public int compareTo(lValue other) {
        if (other == null)
            return 1;
        else if (value instanceof String || other.value instanceof String)
            return asString().compareTo(other.asString());
        else if (value instanceof Double || other.value instanceof Double)
            return Double.compare(asDouble(), other.asDouble());
        else
            return Integer.compare(asInteger(), other.asInteger());
    }

    @Override
    public Copyable<lValue> copy() {
        if (value instanceof String) {
            return new lValue(new String((String)value));
        } else if (value instanceof Double) {
            return new lValue(Double.valueOf((double)value));
        } else if (value instanceof Integer) {
            return new lValue(Integer.valueOf((int)value));
        } else throw new IllegalStateException();
    }
}
