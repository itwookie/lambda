package com.itwookie.lambda;

public class lBlock extends lList {

    @Override
    public Copyable run(Context context) {
        return (Copyable)((lList)super.run(context)).bottom(context, false);
    }

}
