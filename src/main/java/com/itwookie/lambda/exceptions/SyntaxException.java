package com.itwookie.lambda.exceptions;

public class SyntaxException extends RuntimeException {

    public SyntaxException(int line) {
        super("Syntax error at line "+line+": ");
    }

    public SyntaxException(int line, String message) {
        super("Syntax error at line "+line+": "+message);
    }

    public SyntaxException(int line, String message, Throwable cause) {
        super("Syntax error at line "+line+": "+message, cause);
    }

    public SyntaxException(int line, Throwable cause) {
        super("Syntax error at line "+line+": ", cause);
    }
}
