package com.itwookie.lambda;

public class ContextResolver {

    public static Copyable resolve(Context context, Object object) {
        if (object == null) {
            return lValue.VOID;
        } else if (object instanceof lValue) {
            return (lValue)object;
        } else if (object instanceof lLabel) {
            return resolve(context, context.get((lLabel)object));
        } else if (object instanceof Executable) {
            return ((Executable)object).run(context);
//        } else if (object instanceof Callable) {
            //TODO return ((Callable)object).call(context, );
        } else {
            throw new IllegalStateException();
        }
    }

}
