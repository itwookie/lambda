package com.itwookie.lambda;

import java.util.regex.Pattern;

public class lLabel implements Copyable<lLabel> {

    private String label;
    private static final Pattern labelPattern = Pattern.compile("^[\\p{L}_][\\p{L}_0-9]*$");
    private lLabel(String label) {
        this.label = label;
    }

    public static lLabel fromString(String token, boolean allowGlobalFunctionContext) throws IllegalArgumentException {
        if (!labelPattern.matcher(token).matches()) {
            if (!allowGlobalFunctionContext || !FunctionCatalog.containsFunction(token))
                throw new IllegalArgumentException("Invalid label: " + token);
        }
        return new lLabel(token);
    }
    public static lLabel fromString(String token) throws IllegalArgumentException {
        if (!labelPattern.matcher(token).matches())
            throw new IllegalArgumentException("Invalid label: "+token);
        return new lLabel(token);
    }

    @Override
    public String toString() {
        return label;
    }

    public Object resolve(Context context) {
        return context.get(this);
    }

    @Override
    public lLabel copy() {
        return new lLabel(label);
    }
}
