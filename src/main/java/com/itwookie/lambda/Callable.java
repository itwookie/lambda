package com.itwookie.lambda;

public interface Callable {

    Copyable call(Context baseContext, lList passedArgs);

}
