package com.itwookie.lambda;

/** interface for deep copy */
public interface Copyable<T> {

    Copyable<T> copy();

}
