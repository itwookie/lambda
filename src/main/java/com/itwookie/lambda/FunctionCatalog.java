package com.itwookie.lambda;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/** contains function interfacing with the outer world */
public class FunctionCatalog {

    private static Map<String, Callable> functions = new HashMap<>();
    static {
        functions.put("+", (baseContext, passedArgs) -> {
            Copyable c = passedArgs.run(baseContext);
            if (c instanceof lValue) {
                if (((lValue) c).isNumeric()) //force a true numeric copy
                    return lValue.of(((lValue) c).asNumber());
                else
                    throw new IllegalArgumentException("Type exception: string");
            } else if (c instanceof lList) {
                Number val = null;
                Iterator<Copyable> it = ((lList) c).getInterator();
                while (it.hasNext()) {
                    Copyable elem = it.next();
                    if (elem instanceof lValue && ((lValue) elem).isNumeric()) {
                        Number num = ((lValue) elem).asNumber();
                        if (val == null) {
                            val = num;
                        } else if (val instanceof Double || num instanceof Double) {
                            val = val.doubleValue() + num.doubleValue();
                        } else {
                            val = val.intValue() + num.intValue();
                        }
                    }
                }
                return val == null ? lValue.ofInteger(0) : lValue.of(val);
            } else
                throw new IllegalArgumentException("Can't add list with "+c.getClass().getSimpleName()+" elements");
        });

        functions.put("-", (baseContext, passedArgs) -> {
            Copyable c = passedArgs.run(baseContext);
            if (c instanceof lValue) {
                if (((lValue) c).isNumeric()) //force a true numeric copy
                    return lValue.of(((lValue) c).asNumber());
                else
                    throw new IllegalArgumentException("Type exception: string");
            } else if (c instanceof lList) {
                Number val = null;
                Iterator<Copyable> it = ((lList) c).getInterator();
                while (it.hasNext()) {
                    Copyable elem = it.next();
                    if (elem instanceof lValue && ((lValue) elem).isNumeric()) {
                        Number num = ((lValue) elem).asNumber();
                        if (val == null) {
                            val = num;
                        } else if (val instanceof Double || num instanceof Double) {
                            val = val.doubleValue() - num.doubleValue();
                        } else {
                            val = val.intValue() - num.intValue();
                        }
                    }
                }
                return val == null ? lValue.ofInteger(0) : lValue.of(val);
            } else
                throw new IllegalArgumentException("Can't subtract list with "+c.getClass().getSimpleName()+" elements");
        });

        functions.put("*", (baseContext, passedArgs) -> {
            Copyable c = passedArgs.run(baseContext);
            if (c instanceof lValue) {
                if (((lValue) c).isNumeric()) //force a true numeric copy
                    return lValue.of(((lValue) c).asNumber());
                else
                    throw new IllegalArgumentException("Type exception: string");
            } else if (c instanceof lList) {
                Number val = null;
                Iterator<Copyable> it = ((lList) c).getInterator();
                while (it.hasNext()) {
                    Copyable elem = it.next();
                    if (elem instanceof lValue && ((lValue) elem).isNumeric()) {
                        Number num = ((lValue) elem).asNumber();
                        if (val == null) {
                            val = num;
                        } else if (val instanceof Double || num instanceof Double) {
                            val = val.doubleValue() * num.doubleValue();
                        } else {
                            val = val.intValue() * num.intValue();
                        }
                    }
                }
                return val == null ? lValue.ofInteger(0) : lValue.of(val);
            } else
                throw new IllegalArgumentException("Can't subtract list with "+c.getClass().getSimpleName()+" elements");
        });

        functions.put("/", (baseContext, passedArgs) -> {
            Copyable c = passedArgs.run(baseContext);
            if (c instanceof lValue) {
                if (((lValue) c).isNumeric()) //force a true numeric copy
                    return lValue.of(((lValue) c).asNumber());
                else
                    throw new IllegalArgumentException("Type exception: string");
            } else if (c instanceof lList) {
                Number val = null;
                Iterator<Copyable> it = ((lList) c).getInterator();
                while (it.hasNext()) {
                    Copyable elem = it.next();
                    if (elem instanceof lValue && ((lValue) elem).isNumeric()) {
                        Number num = ((lValue) elem).asNumber();
                        if (val == null) {
                            val = num;
                        } else if (val instanceof Double || num instanceof Double) {
                            val = val.doubleValue() / num.doubleValue();
                        } else {
                            val = val.intValue() / num.intValue();
                        }
                    }
                }
                return val == null ? lValue.ofInteger(0) : lValue.of(val);
            } else
                throw new IllegalArgumentException("Can't subtract list with "+c.getClass().getSimpleName()+" elements");
        });

        functions.put("%", (baseContext, passedArgs) -> {
            Copyable c = passedArgs.run(baseContext);
            if (c instanceof lValue) {
                if (((lValue) c).isNumeric()) //force a true numeric copy
                    return lValue.of(((lValue) c).asNumber());
                else
                    throw new IllegalArgumentException("Type exception: string");
            } else if (c instanceof lList) {
                Number val = null;
                Iterator<Copyable> it = ((lList) c).getInterator();
                while (it.hasNext()) {
                    Copyable elem = it.next();
                    if (elem instanceof lValue && ((lValue) elem).isNumeric()) {
                        Number num = ((lValue) elem).asNumber();
                        if (val == null) {
                            val = num;
                        } else if (val instanceof Double || num instanceof Double) {
                            val = val.doubleValue() % num.doubleValue();
                        } else {
                            val = val.intValue() % num.intValue();
                        }
                    }
                }
                return val == null ? lValue.ofInteger(0) : lValue.of(val);
            } else
                throw new IllegalArgumentException("Can't subtract list with "+c.getClass().getSimpleName()+" elements");
        });

        functions.put("==", (baseContext, passedArgs)->{
            Copyable c = passedArgs.run(baseContext);
            if (c instanceof lValue) {
                if (((lValue) c).isNumeric()) //force a true numeric copy
                    return lValue.of(((lValue) c).asNumber());
                else
                    throw new IllegalArgumentException("Type exception: string");
            } else if (c instanceof lList) {
                Number val = null;
                Iterator<Copyable> it = ((lList) c).getInterator();
                while (it.hasNext()) {
                    Copyable elem = it.next();
                    if (elem instanceof lValue && ((lValue) elem).isNumeric()) {
                        Number num = ((lValue) elem).asNumber();
                        if (val == null) {
                            val = num;
                        } else if (val instanceof Double || num instanceof Double) {
                            val = val.doubleValue() % num.doubleValue();
                        } else {
                            val = val.intValue() % num.intValue();
                        }
                    }
                }
                return val == null ? lValue.ofInteger(0) : lValue.of(val);
            } else
                throw new IllegalArgumentException("Can't subtract list with "+c.getClass().getSimpleName()+" elements");
        });
    }

    public static Callable find(lLabel key) {
        return functions.get(key.toString().toLowerCase());
    }

    public static void register(String key, Callable function) {
        if (functions.containsKey(key.toLowerCase()))
            throw new IllegalArgumentException("Lambda with name "+key+" already registered");
        functions.put(lLabel.fromString(key).toString().toLowerCase(), function);
    }
    public static boolean containsFunction(String key) {
        return functions.containsKey(key.toLowerCase());
    }

}
