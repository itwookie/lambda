package com.itwookie.lambda;

public interface Executable {

    Copyable run(Context context);

}
