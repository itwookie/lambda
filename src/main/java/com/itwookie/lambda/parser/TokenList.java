package com.itwookie.lambda.parser;

import com.itwookie.lambda.exceptions.SyntaxException;
import com.itwookie.lambda.lLabel;
import com.itwookie.lambda.lValue;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class TokenList extends LinkedList<Object> {

    public enum Category {
        ROOT,
        ARGS, // (,)
        LIST, // [,]
        BLOCK, // {,}
        VALUE_INDEX, //list[,]
        REF_INDEX, //list{,}
        NUMSPAN, //[:]
        UNKNWON,
    }

    private Category category=null;
    private TokenList parent;
    private int line=1;

    public TokenList(TokenList parent) {
        if (parent == null) category = Category.ROOT;
        this.parent = parent;
    }

    public TokenList getParent() {
        if (parent == null)
            throw new NoSuchElementException();
        return parent;
    }

    public TokenList getRoot() {
        TokenList list = this;
        while (list.parent != null)
            list = list.parent;
        return list;
    }

    public boolean isRoot() {
        return parent == null;
    }

    public void splice(int from, int length, Object... insert) {
        removeRange(from, from+length);
        addAll(from, Arrays.asList(insert));
    }

    /**
     * @param allowElse interprets : as separator for numspan lists
     * @return index of next separator token if any or -1
     */
    public int indexOfSeparator(boolean allowElse) {
        for (int i = 0; i < size(); i++) {
            Object o = get(i);
            if (o instanceof Token) {
                Token t = (Token)o;
                if (t.getType().equals(Token.Type.SEPARATOR) ||
                    (t.getType().equals(Token.Type.ELSE) && allowElse)) {
                    return i;
                }
            }
        }
        return -1;
    }
    public int indexOfType(Token.Type type) {
        for (int i = 0; i < size(); i++) {
            Object o = get(i);
            if (o instanceof Token) {
                Token t = (Token)o;
                if (t.getType().equals(type)) {
                    return i;
                }
            }
        }
        return -1;
    }
    public int getFirstSourceLine() {
        return line;
    }

    /** only returns valid results while building token-lists */
    private Object getPreviousSibling() {
        if (isRoot())
            return null;
        if (getParent().size()<2) //we're the first entry
            return null;

        return getParent().get(getParent().size()-2); //not the last element (this), but the previous
    }

    /**
     * This function only validates lLists. not that a,b,c is also a TokenList of 5, but
     * not a lList that could be validated by this function.<br>
     * This function won't correctly detect numspans.
     * @throws SyntaxException if the user did not properly terminate the list
     * @return true
     */
    public boolean refinelList() {
        if (category!=null)
            return true;
        if ( !(getFirst() instanceof Token) )
            throw new IllegalStateException("List parser needs to insert list start token");
        if ( !(getLast() instanceof Token) )
            throw new IllegalStateException("List parser needs to append list end token");
        Token first = (Token) getFirst();
        Token last = (Token) getLast();
        line = first.getSourceLine();

        if ( first.getType().equals(Token.Type.ARGS_OPEN) ) {
            if ( !last.getType().equals(Token.Type.ARGS_CLOSE) )
                throw new SyntaxException(last.getSourceLine(), "Args opened, but never closed");
            category = Category.ARGS;
        } else if ( first.getType().equals(Token.Type.LIST_OPEN) ) {
            if ( !last.getType().equals(Token.Type.LIST_CLOSE) )
                throw new SyntaxException(last.getSourceLine(), "List/Index opened, but never closed");
            Object ps = getPreviousSibling();
            if ( ps instanceof TokenList || (ps instanceof Token && ((Token)ps).getType().equals(Token.Type.LABEL)) ) {
                category = Category.VALUE_INDEX;
            } else {
                category = Category.LIST;
                int i = indexOfSeparator(true);
                if (i>=0) {
                    Object o = get(i);
                    if (o instanceof Token) {
                        Token t = (Token) o;
                        if (t.getType().equals(Token.Type.ELSE)) {
                            category = Category.NUMSPAN;
                        }
                    }
                }
            }
        } else if ( first.getType().equals(Token.Type.BLOCK_OPEN) ) {
            if ( !last.getType().equals(Token.Type.BLOCK_CLOSE) )
                throw new SyntaxException(last.getSourceLine(), "Block opened, but never closed");
            Object ps = getPreviousSibling();
            if ( ps instanceof TokenList || (ps instanceof Token && ((Token)ps).getType().equals(Token.Type.LABEL)) ) {
                category = Category.REF_INDEX;
            } else {
                category = Category.BLOCK;
            }
        } else {
            throw new IllegalStateException("How did I get here? No really, this is a critical parse error!");
        }

        removeFirst();
        removeLast();
        //mutations that do not change the length and make further parsing easier
        for (int i = 0; i < size(); i++) {
            Object o = get(i);
            if (o instanceof Token) {
                Token t = (Token) o;
                if (t.getType().equals(Token.Type.NUMBER) ||
                    t.getType().equals(Token.Type.STRING)) {
                    splice(i,1, lValue.fromString(t.getRaw()));
                } else if (t.getType().equals(Token.Type.LABEL)) {
                    splice(i,1, lLabel.fromString(t.getRaw(), true));
                }
            }
        }
        return true;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String toString() {
        return _toString("");
    }
    private String _toString(String padding) {
        String subPadding = padding + " ";
        StringBuilder sb = new StringBuilder();
        sb.append(padding)
            .append("< ")
            .append(category.name())
            .append('\n');
        forEach(x->{
            if (x instanceof TokenList)
                sb.append( ((TokenList)x)._toString(subPadding) );
            else if (x instanceof Token)
                sb.append(subPadding)
                    .append( x.toString() );
            else {
                sb.append(subPadding)
                    .append("*")
                    .append( x.getClass().getSimpleName() )
                    .append(": ")
                    .append( x.toString() );
            }
            sb.append('\n');
        });
        sb.append(padding)
            .append(">");
        return sb.toString();
    }

}
