operators: +(sum over all elements), 
           -(continuous substraction from first element),
           *(multiplication over all elements),
           /(continuous division of first element),
           %(continuous moulo of first element),
           ~(bitwise negate (as integer) the list),
           !(return the negation of top)
           ^(return value of first two elements xored),
           <<(shift first element left n bits or ltrim the string at first element by n characters, where n is the second value in the list as integer),
           >>(shift first element right n bits or rtrim the string at first element by n characters, where n is the second value in the list as integer),
           >>>(like >> but does NOT keep the signum for bit shifting),
           |(continuous bitwise of the the first element),
           ||(logically OR compare all elements in the list, numbers != 0 are true, strings are true, 0 and VOID are false)
           &(continuous bitwise of the the first element),
           &&(logicaly AND compare all elements in the list, numbers != 0 are true, strings are true, 0 and VOID are false)
		   <=(each following value in the list has to be less-equal the previous one)
		   >=(each following value in the list has to be greater-equal the previous one)
		   !=(each following value has to be different -> #set == #list)
           ;(no operation)
		   #(length of string, list or block)
		   set(remove duplicated from list)
		   
label: variable name or function name, must match ^[\p{L}_$][\p{L}_$0-9]*$
primitive: number, string
data: PRIMITIVE or LIST
value: FUNCTION CALLS, FUNCTION DEFINITIONS, EXPRESSIONS, DATA or varaiable name
expression: a string of VALUES and operators that return a VALUE
list: collection of EXPRESSIONS
stream: list of two elements: [ (amount)=>list, (list)=>; ], reading amount elements into a list, writing all elements from a list. amount of < 0 means read all
void: special variable, like /dev/null or :null, write only, no value
top: first VALUE in a LIST (list gets evaluated first)
bottom: last VALUE in a LIST (list gets evaluated first)
collect: valid only in arguments, collect values up to the lsit termination into a LIST (variable-name...)
spread: spread the first N elements in the list across all N arguments in the function, stopping after FLOOR(#list, #arguments) (...list)
result:
    value: in function a expression, in assignment a name (map results as list to label result)
    [list]: in function a list of instructions (function returns array with result of each step), in assignment maping the N th entry in list to the N th label in 
    {list}: in function a list of instructions (function returns last value), in assignment set label to be the bottom
value copy: copy all N entries in the list to N entries in a new list by value ([...list])
value access: applies to LISTS, access nth VALUE, ==[n,1]
value sublist: applies to LISTS, new sublist with up to l elements starting at n, negative l reverse output direction, negative l with n==0 start at end
value splice: applies to LISTS, list, like [n,l], x is a COLLECT where all VALUES get placed at n
expression copy: like value copy, does not evaluate ({...list}), PRIMITIVES are still copied
expression access: like value access, does not evaluate, PRIMITIVES are still copied
expression sublist: like value sublist, does not evaluate, PRIMITIVES are still copied
expression splice: like value splice, does not evaluate, PRIMITIVES are still copied

function            label,function-name(list,arguments)=>list/block
anon function       (list,arguments)=>list/block
invocation          label(list,arguments)
assignment          ->result
stream              @[(amount)=>value, (value...)=>;]
condition           (top,condition) ? list,true : list,false
spread              list...
collect             ...list
foreach             list/stream:anon function/function
list                [ EXPRESSION, EXPRESSION, EXPRESSION, ... ]
block               { EXPRESSION, EXPRESSION, EXPRESSION, ... }
length              #list OR #block
value-access        ]}[n]
value-sublist       ]}[n,l]
value-splice        ]}[n,l,x...]
expression-access   ]}{n}
expression-sublist  ]}{n,l}
expression-splice   ]}{n,l,x...}

predefined functions: 
  read(stream, amount)->list
      access stream supply
  readLine(stream)->line
      access stream supply, reading until 0x10
  fetch(stream, amount)->list
      access stream supply, reading up to amount bytes
  skip(stream, amount)->;
      access stream supply, ignoring up to amount bytes
  print(stream, list)->;
      access stream sink, writing all elements as string
  printLine(stream, ...elements)->;
      access stream sink, writing all elements as string, followed by 0x10
  write(stream, list)->;
      access stream sink, writing strings as strings, numbers cast to integer as single bytes & 0xff
  console()->stream
      returns a stream of [ system in, system out ]
	  
Tokens:
 ( token, ...)
 [ token, ...]
 { token, ...}
 "text"
 Number (decimal, binary, hex)
 token => token
 token -> token
 token ? token : token
 operator token