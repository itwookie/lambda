package com.itwookie.lambda.parser;

public class Token {

    public enum Type {
        ARGS_OPEN, // (
        ARGS_CLOSE, // )
        LIST_OPEN, // [
        LIST_CLOSE, // ]
        BLOCK_OPEN, // {
        BLOCK_CLOSE, // }
        SEPARATOR, // ,
        STRING, // ""
        NUMBER, // 123
        LABEL, // abc
        LAMBDA, // =>
        ASSIGN, // ->
        THEN, // ?
        ELSE, // :
        NOP, // ;
        SPREAD, // ...
        COUNT, // #
    }

    private int line;
    private String raw;
    private Type type;

    public Token(int originatingLine, Type type, String raw) {
        this.line = originatingLine;
        this.raw = raw;
        this.type = type;
    }

    public Type getType() {
        return type;
    }
    public String getRaw() {
        return raw;
    }
    public int getSourceLine() { return line; }

    @Override
    public String toString() {
        return type.name() + ": " + raw;
    }
}
