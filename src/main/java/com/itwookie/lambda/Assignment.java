package com.itwookie.lambda;

public class Assignment implements Copyable<Assignment>, Executable {

    private Copyable left;
    private lLabel right;
    public Assignment(Copyable left, lLabel right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Copyable run(Context context) {
        Copyable value = ContextResolver.resolve(context, left);
        context.set(right, value);
        return value;
    }

    @Override
    public Copyable<Assignment> copy() {
        return new Assignment(left.copy(), right.copy());
    }
}
