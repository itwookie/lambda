package com.itwookie.lambda;

import java.util.HashMap;
import java.util.Map;

public class Context {

    private Map<String, Object> data = new HashMap<>();
    private Context parent;

    public Context(Context parent) {
        this.parent = parent;
    }
    public Context() {
        this.parent = null;
    }

    public Object get(lLabel label) {
        Object object = data.get(label.toString().toLowerCase());
        if (object == null) {
            if (parent != null)
                return parent.get(label);
            else {
                return FunctionCatalog.find(label);
            }
        }
        return object;
    }

    public void set(lLabel label, Object value) {
        String l = label.toString().toLowerCase();
        context(l)._set(l, value);
    }
    public void force(lLabel label, Object value) {
        _set(label.toString().toLowerCase(), value);
    }

    private void _set(String label, Object value) {
        data.put(label, value);
    }
    private Context context(String label) {
        Context c = _context(label);
        return c==null?this:c;
    }
    private Context _context(String label) {
        if (data.containsKey(label)) return this;
        if (parent != null) {
            return parent._context(label);
        }
        return null;
    }

}
