package com.itwookie.lambda.parser;

import com.itwookie.lambda.Runtime;
import com.itwookie.lambda.exceptions.ParseException;
import com.itwookie.lambda.exceptions.SyntaxException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Parser {

    static class Scanner implements Iterator<Token> {
        private int cno = 0;
        private int lno = 1;
        private InputStreamReader in;
        Scanner(InputStream in) throws IOException {
            try {
                this.in = new InputStreamReader(in, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            lookAhead();
        }

        private Token next = null;

        @Override
        public boolean hasNext() {
            return next != null;
        }

        @Override
        public Token next() {
            if (!hasNext())
                throw new NoSuchElementException();
            Token t = next;
            try {
                lookAhead();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return t;
        }

        private int readChar() throws IOException {
            int c = in.read();
            cno++;
            if (c < 0) next = null;
            if (c == '\n') {
                cno =1;
                lno++;
            }
            return c;
        }

        private Character bonus = null;
        private void lookAhead() throws IOException {
            int r;
            char first = bonus == null ? ' ' : bonus;
            bonus = null;
            next = null;
            while (Character.isWhitespace(first)) {
                if ((r = readChar())<0) return;
                first = (char) r;
            }
            // lists
            if (first == '(') {
                next = new Token(lno, Token.Type.ARGS_OPEN, "(");
            } else if (first == ')') {
                next = new Token(lno, Token.Type.ARGS_CLOSE, ")");
            } else if (first == '[') {
                next = new Token(lno, Token.Type.LIST_OPEN, "[");
            } else if (first == ']') {
                next = new Token(lno, Token.Type.LIST_CLOSE, "]");
            } else if (first == '{') {
                next = new Token(lno, Token.Type.BLOCK_OPEN, "{");
            } else if (first == '}') {
                next = new Token(lno, Token.Type.BLOCK_CLOSE, "}");
            } else if (first == ',') {
                next = new Token(lno, Token.Type.SEPARATOR, ",");
            } else if (first == '"') { //strings
                StringBuilder sb = new StringBuilder();
                boolean closed = false;
                boolean escaped = false;
                while ((r = readChar()) >= 0) {
                    char c = (char) r;
                    if (escaped) {
                        if (c == '\\')
                            sb.append('\\');
                        else if (c == 'n')
                            sb.append('\n');
                        else if (c == 'r')
                            sb.append('\r');
                        else if (c == 't')
                            sb.append('\t');
                        else if (c == '"')
                            sb.append('"');
                        else throw new ParseException(lno, cno, "Illegal escape sequence \\" + c);
                        escaped = false;
                    } else if (c == '\\') {
                        escaped = true;
                    } else if (c == '"') {
                        closed = true;
                        break;
                    } else {
                        sb.append(c);
                    }
                }
                if (!closed)
                    throw new ParseException(lno, cno, "How did i get here?");

                next = new Token(lno, Token.Type.STRING, sb.toString());
            } else if (first >= '0' && first <= '9') { //positive numbers
                StringBuilder sb = new StringBuilder();
                sb.append(first);
                boolean dot = false;
                while ((r = readChar()) >= 0) {
                    char c = (char) r;
                    if ((c == 'b' || c == 'x' && sb.length() == 1) ||
                            (c >= '0' && c <= '9')
                            ) {
                        sb.append(c);
                    } else if (c == '.') {
                        if (dot) throw new ParseException(lno, cno, new NumberFormatException("Numbers can't contain more than 1 decimal"));
                        dot = true;
                        sb.append(c);
                    } else if (c != '_') {
                        //allow _ as spacer

                        bonus = c;
                        break; //number is over
                        //throw new ParseException(lno, cno, new NumberFormatException());
                    }
                }
                next = new Token(lno, Token.Type.NUMBER, sb.toString());
            } else if (Character.isLetter(first) || first == '_' || first == '$') { //labels
                StringBuilder sb = new StringBuilder();
                sb.append(first);
                while ((r = readChar()) >= 0) {
                    char c = (char) r;
                    if (Character.isLetter(c) || (c >= '0' && c <= '9') || first == '_' || first == '$')
                        sb.append(c);
                    else {
                        bonus = c;
                        break; // identifier is over //throw new ParseException(lno, cno, "Illegal character "+c+" in identifier");
                    }
                }
                next = new Token(lno, Token.Type.LABEL, sb.toString());
            } else if (first == '.') {
                if ((r = readChar()) >= 0) {
                    if ((char)r == '.') {
                        if ((r = readChar()) >= 0) {
                            if ((char)r == '.') {
                                next = new Token(lno, Token.Type.SPREAD, "...");
                            } else {
                                throw new ParseException(lno, cno, "Free roaming dots need to go in groups of three");
                            }
                        }
                    } else {
                        throw new ParseException(lno, cno, "Free roaming dots need to go in groups of three");
                    }
                }
            } else if (first == '=') { //equality n fat arrows
                if ((r = readChar()) >= 0) {
                    if ((char)r == '>') {
                        next = new Token(lno, Token.Type.LAMBDA, "=>");
                    } else if ((char)r != '=') {
                        throw new ParseException(lno, cno, "Illegal token, > or = expected");
                    } else {
                        if ((r = readChar()) >= 0) {
                            if ((char)r == '=') {
                                next = new Token(lno, Token.Type.LABEL, "===");
                            } else {
                                next = new Token(lno, Token.Type.LABEL, "==");
                                bonus = (char)r;
                            }
                        }
                    }
                }
            } else if (first == '-') { //negative numbers and thin arrows
                //TODO negative zahlen
                if ((r = readChar()) >= 0) {
                    if ((char)r == '>') {
                        next = new Token(lno, Token.Type.ASSIGN, "->");
                    } else {
                        next = new Token(lno, Token.Type.LABEL, "-");
                        bonus = (char)r;
                    }
                }
            } else if (first == '/') { //stop, comment time
                if ((r = readChar()) >= 0) {
                    if ((char)r == '/') {
                        //line comment init
                        while ( (r = readChar())!='\n' && r!='\r' )
                            if (r<0) return;
                        lookAhead(); //look for something after comment
                    } else if ((char)r == '*') {
                        //block comment init
                        char p = (char)r;
                        while ( (r = readChar()) != '/' || p != '*' ) {
                            if (r<0) return;
                            p = (char)r;
                        }
                        lookAhead(); //look for something after comment
                    } else {
                        next = new Token(lno, Token.Type.LABEL, "/");
                        bonus = (char)r;
                    }
                }
            } else if (first == '<') { //leq, lss, left shift
                if ((r = readChar()) >= 0) {
                    if ((char)r == '=') {
                        next = new Token(lno, Token.Type.LABEL, "<=");
                    } else if ((char)r == '<') {
                        next = new Token(lno, Token.Type.LABEL, "<<");
                    } else {
                        next = new Token(lno, Token.Type.LABEL, "<");
                        bonus = (char)r;
                    }
                }
            } else if (first == '>') { //grtr, geq, rhift, sigless shift
                if ((r = readChar()) >= 0) {
                    if ((char)r == '=') {
                        next = new Token(lno, Token.Type.LABEL, ">=");
                    } else if ((char)r == '>') {
                        if ((r = readChar()) >= 0) {
                            if ((char)r == '>') {
                                next = new Token(lno, Token.Type.LABEL, ">>>");
                            } else {
                                next = new Token(lno, Token.Type.LABEL, ">>");
                                bonus = (char)r;
                            }
                        }
                    } else {
                        next = new Token(lno, Token.Type.LABEL, ">");
                        bonus = (char)r;
                    }
                }
            } else if (first == '!') { //neq, super neq, not
                if ((r = readChar()) >= 0) {
                    if ((char)r == '=') {
                        if ((r = readChar()) >= 0) {
                            if ((char)r == '=') {
                                next = new Token(lno, Token.Type.LABEL, "!==");
                            } else {
                                next = new Token(lno, Token.Type.LABEL, "!=");
                                bonus = (char)r;
                            }
                        }
                    } else {
                        next = new Token(lno, Token.Type.LABEL, "!");
                        bonus = (char)r;
                    }
                }
            } else if (first == '|') { // or?
                if ((r = readChar()) >= 0) {
                    if ((char)r == '|') {
                        next = new Token(lno, Token.Type.LABEL, "||");
                    } else {
                        next = new Token(lno, Token.Type.LABEL, "|");
                        bonus = (char)r;
                    }
                }
            } else if (first == '&') { // and?
                if ((r = readChar()) >= 0) {
                    if ((char)r == '&') {
                        next = new Token(lno, Token.Type.LABEL, "&&");
                    } else {
                        next = new Token(lno, Token.Type.LABEL, "&");
                        bonus = (char)r;
                    }
                }
            } else if (first == '?') {
                next = new Token(lno, Token.Type.THEN, "?");
            } else if (first == ':') {
                next = new Token(lno, Token.Type.ELSE, ":"); //might be separator in numspan
            } else if (first == ';') {
                next = new Token(lno, Token.Type.NOP, ";");
            } else if (first == '+' || first == '*' || first == '%' || first == '~' || first == '^') {
                next = new Token(lno, Token.Type.LABEL, String.valueOf(first));
            } else if (first == '#') {
                next = new Token(lno, Token.Type.COUNT, "#");
            } else {
                throw new ParseException(lno, cno, "Invalid Token: ");
            }
            if (next == null)
                throw new ParseException(lno, cno, "Could not parse next Token");
        }
    }

    public Runtime parse (InputStream in) throws IOException {
        return parse(new Scanner(in));
    }
    private Runtime parse (Scanner tokens) {
        //collects tokens and lists of tokens
        TokenList list = new TokenList(null);

        //collect all lists
        while (tokens.hasNext()) {
            Token token = tokens.next();

            if (token.getType().equals(Token.Type.ARGS_OPEN) ||
                token.getType().equals(Token.Type.LIST_OPEN) ||
                token.getType().equals(Token.Type.BLOCK_OPEN) ) {

                TokenList child = new TokenList(list);
                child.add(token);
                list.add(child);
                list = child;
            } else {
                list.add(token);
                if (token.getType().equals(Token.Type.ARGS_CLOSE) ||
                    token.getType().equals(Token.Type.LIST_CLOSE) ||
                    token.getType().equals(Token.Type.BLOCK_CLOSE) ) {

                    list.refinelList(); //now has a type

                    if (list.isRoot()) {
                        break; //root list closed (usually impossible at this position)
                    } else {
                        list = list.getParent();
                    }
                }
            }
        }
        while (!list.isRoot()) {
            TokenList child = list;
            list = list.getParent();
            list.add(child);
        }

        //validate syntax and convert to executable stuff
        // *note*:
        //  lambda in this context can come from a variable or be a declaration.
        //  list in this context can be a variable or a declaration or a single
        //  expression that auto-wraps in a singleton list.
        //lambda:
        //  (args)=>list
        //name lambda:
        //  label lambda-declaration
        //invocation:
        //  lambda(args)
        //assign:
        //  ###->label
        //condition: (and permutations)
        //  list?list:list
        //spread, collect: (low priority)
        //  ...list
        //  list...
        //foreach:
        //  list:lambda
        //length:
        //  #list
        //index: (higher priority)
        //  list literal-list
        //numspan:
        //  []list with colon separator

        System.out.println(list.toString());
        return null;
    }

}
