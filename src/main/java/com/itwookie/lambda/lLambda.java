package com.itwookie.lambda;

import java.util.LinkedList;
import java.util.List;

public class lLambda implements Copyable<lLambda>, Callable {

    private List<lLabel> orderdArgumentNames;
    private lList instructions;

    public lLambda(List<lLabel> args, lList body) {
        this.orderdArgumentNames = args;
        this.instructions = body;
    }

    @Override
    public Copyable call(Context baseContext, lList passedArgs) {
        Context bodyContext = new Context(baseContext);
        for (int i = 0; i < orderdArgumentNames.size() && i < passedArgs.length(); i++) {
            bodyContext.force(orderdArgumentNames.get(i), passedArgs.get(baseContext, i, true));
        }
        return instructions.run(bodyContext);
    }

    @Override
    public lLambda copy() {
        List<lLabel> clabel = new LinkedList<>();
        for (lLabel l : orderdArgumentNames) clabel.add(l.copy());
        return new lLambda(clabel, instructions.copy());
    }
}
