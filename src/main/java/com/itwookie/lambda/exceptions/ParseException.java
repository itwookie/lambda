package com.itwookie.lambda.exceptions;

public class ParseException extends RuntimeException {

    public ParseException() {
        super();
    }

    public ParseException(int line, int column, String message) {
        super("Parse Error at line "+line+", column "+column+": "+message);
    }

    public ParseException(int line, int column, String message, Throwable exception) {
        super("Parse Error at line "+line+", column "+column+": "+message, exception);
    }

    public ParseException(int line, int column, Throwable exception) {
        super("Parse Error at line "+line+", column "+column+":", exception);
    }

}
